---
template: overrides/main.html
---

<h1> {{ mode }} -  {{ operation_modes.loc[mode, 'SHORT_DESCRIPTION'] }} </h1>

## Twiss functions
The Twiss functions of this operation mode are shown in the interactive plot below. 
You can zoom in or hover over any curve to obtain more information about the function's value at a specific element. 
Below the plot, the Twiss table can be downloaded as TFS file. 

!!! warning "Loading of plots"
    Due to the large number of elements in FCC-ee, the loading of the plots may take a minute or two. 

<object width="90%" height="{{plot_height}}" data="{{optics_plot}}"></object> 

!!! note "Twiss table"
    The TFS table can be directly downloaded in [TFS]({{ tfs_file }}) format.

!!! note "Survey table"
    The survey of the sequence can be directly downloaded in [TFS]({{ survey_file }}) format.

!!! note "MAD-X"
    This [MAD-X example script]({{ madx_file }}) is used to create the twiss file and produces this [MAD-X sequence file]({{ sequence_file }}) for the {{ mode }} operation mode.

!!! note "SAD"
    The optics can be checked using this [SAD script]({{ sad_optics_file }}). An example how to perform DA studies using SAD can be found [here]({{ sad_da_file }}).
