# FCC-ee optics repository web directory

This directory holds scripts and templates to create the [acc-models](https://acc-models.web.cern.ch/acc-models/fcc/) webpage.

The data with which these pages will be filled needs to be generated first.

Using `run_codes.py`, the templates in the `tests` directory will be run, and the data will be saved in the `public` folder, with subdirectories for the different operation modes.

Then, running `create_web.py` with the option `--branch MY_BRANCH` fills out the markdown templates found in the `templates` directory and
the optics plots.

The filled out pages will be stored in the `public` directory.

Note that for doing so, some python packages may need to be installed.
These can be conveniently done by issuing `pip install -r python_requirements.txt`.

Changes to the website can be made by changing the files in the `template` directory.
In case of adding more variables to the template, don't forget to add those also accordingly in the `fill_template` function in `create_web.py`.
