import jinja2
import json
import tfs
from generic_parser import EntryPointParameters, entrypoint
from pathlib import Path
import pandas as pd
import numpy as np

from bokeh.plotting import figure, output_file, save, ColumnDataSource
from bokeh.models import Range1d, LinearAxis
from bokeh.layouts import column


REPOSITORY_TOP_LEVEL = Path(__file__).resolve().parent.parent
REFERENCE_FILE = json.load(open(REPOSITORY_TOP_LEVEL/"reference_parameters.json"))
OPERATION_MODES = REFERENCE_FILE['OPERATION_MODES']
TEMPLATE_DIRECTORY = REPOSITORY_TOP_LEVEL/'web'/'templates'

WEBSITE_DIRECTORY = Path('/eos/project/a/acc-models/public/fcc/fccee')

ELEMENT_STYLES = [
    {'name':'BEND',                 'offset':0.0,   'height':1.2, 'color':'#2ca25f' },
    {'name':'SEXTUPOLE',           'offset':0.4,   'height':0.8, 'color':'#fff7bc' },
    {'name':'KICKER',               'offset':0.0,   'height':2.0, 'color':'#1b9e77' },
    {'name':'CAVITY',               'offset':0.7,   'height':1.0, 'color':'#e7e1ef' },
    {'name':'CAVITY',               'offset':-0.7,  'height':1.0, 'color':'#e7e1ef' },
    {'name':'MONITOR|INSTRUMENT',   'offset':0.0,   'height':2.0, 'color':'gray'    },
    {'name':'SOLENOID',             'offset':0.0,   'height':0.5, 'color':'#a4a4a4' },
]

PARAMETER_WITH_UNITS={
        "ENERGY": r"$Energy~[GeV]$",
        "EMITTANCE_X": r"$\epsilon_x~[m]$",
        "EMITTANCE_Y": r"$\epsilon_y~[m]$",
        "EMITTANCE_RATIO": r"$\epsilon_y/\epsilon_x$",
        "BUNCHES": r"$N_{bunches}$",
        "BUNCH_POPULATION": r"$Bunch~population$",
        "BEAM_CURRENT": r"$Beam~current~[mA]$",
        "MOMENTUM_COMPACTION": r"$Momentum~compaction~\alpha_p$",
        "Q1": r"$Q_x$",
        "Q2": r"$Q_y$",
        "BETASTAR_X": r"$\beta_x^*~[m]$",
        "BETASTAR_Y": r"$\beta_y^*~[m]$" 
}

# Script arguments -------------------------------------------------------------
def get_params():
    params = EntryPointParameters()
    params.add_parameter(
        name="branch",
        type=str,
        required=True,
        help="Name of the deployed branch.",
    )
    return params


@entrypoint(get_params(), strict=True)
def main(opt):

    # fill out the navigation yml for the branch
    fill_template("nav.yml.template",
                  REPOSITORY_TOP_LEVEL/'public'/'nav.yml',
                  {"branch":opt.branch,
                   'operation_modes':OPERATION_MODES
                   })

    # load the json file and convert in a pandas df
    # the df is then transformed into an mk table and fills the parameter template
    operation_modes = pd.json_normalize([REFERENCE_FILE[key] for key in OPERATION_MODES])
    operation_modes.index=OPERATION_MODES
    
    with open(REPOSITORY_TOP_LEVEL/'CHANGELOG.md') as f:
        changelog=f.read()

    fill_template("parameters.template",
                  REPOSITORY_TOP_LEVEL/'public'/'index.md',
                  {"branch":opt.branch,
                   "changelog":changelog,
                #    use the data from the reference json, but discards any entry with DESCRIPTION in the name and entry with sublevels
                   'operation_mode_table':operation_modes.loc[:, ~operation_modes.columns.str.contains('.*\..*|DESCRIPTION$')].transpose().rename(index=PARAMETER_WITH_UNITS).to_markdown(tablefmt="github")
                   })

    # for each of the operation modes, the output from the tests is used to create bokeh optics plots
    # and based on the operation mode template a website with the plots included is created
    for mode in OPERATION_MODES:
        path_mode = REPOSITORY_TOP_LEVEL/'public'/mode
        tfs_file = f'twiss_{mode}_b1_nottapered.tfs'
        aperture_file = f'aperture_{mode}_b1_nottapered.tfs'
        optics_plot = create_bokeh_optics_plot(tfs.read(path_mode/tfs_file),
                                               tfs.read(path_mode/aperture_file),
                                               REPOSITORY_TOP_LEVEL/'public'/mode/f'optics_{mode}.html')
        plot_height = '1300'
        fill_template("operation_mode.template",
                      path_mode/'index.md',
                      {"mode":mode,
                       'operation_modes':operation_modes,
                       'optics_plot':f'optics_{mode}.html',
                       'plot_height':plot_height,
                       'tfs_file':tfs_file,
                       'madx_file':'madx.rendered',
                       'sequence_file':f'fcc_ee_{mode}_b1_tapered.seq',
                       'survey_file':f'survey_madx_{mode}_b1.tfs',
                       'sad_optics_file':f'sad_optics.rendered',
                       'sad_da_file':f'sad_da.rendered',
                      })


def fill_template(template_file, path, renderdata):
    loader = jinja2.FileSystemLoader(searchpath=TEMPLATE_DIRECTORY)
    env = jinja2.Environment(loader=loader, undefined=jinja2.StrictUndefined)
    template = env.get_template(template_file)
    with open(path, 'w') as f:
        f.write(template.render(**renderdata))


def create_bokeh_optics_plot(optics_df, aperture_df, file_path):

    # definition of parameters to be shown when hovering the mouse over the data points
    tooltips = [("parameter", "$name"), ("element", "@NAME"), ("value [m]", "$y")]
    tooltips_bsc = [("parameter", "$name"), ("element", "@NAME"), ("value [\u03c3]", "$y")]
    tooltips_elements = [("element", "@NAME"), ("Length [m]", "@L")]

    optics = ColumnDataSource(optics_df)
    aperture = ColumnDataSource(aperture_df)

    optics_plot = figure(title="",
                         x_axis_label='s [m]',
                         y_axis_label=u'\u03B2-functions [m]',
                         width=1000,
                         height=500,
                         x_range=Range1d(0, optics_df.LENGTH, bounds="auto"),
                         y_range=Range1d(0, optics_df[['BETX', 'BETY']].max().max()*1.1),
                         tools="box_zoom, pan, reset, hover, undo, zoom_in, zoom_out",
                         active_drag = 'box_zoom',
                         tooltips = tooltips)

    optics_plot.axis.major_label_text_font = 'times'
    optics_plot.axis.axis_label_text_font = 'times'
    optics_plot.axis.axis_label_text_font_style = 'normal'
    optics_plot.outline_line_color = 'black'
    optics_plot.sizing_mode = 'scale_width'

    optics_plot.line('S', 'BETX', source=optics, name=u"\u03B2x", line_width=1.5, line_color='red')
    optics_plot.line('S', 'BETY', source=optics, name=u"\u03B2y", line_width=1.5, line_color='blue')

    dispersion_plot = figure(title="",
                             x_axis_label='s [m]',
                             y_axis_label='dispersion-function [m]',
                             width=1000,
                             height=200,
                             x_range=optics_plot.x_range,
                             y_range=Range1d(optics_df['DX'].min()*1.1 if np.sign(optics_df['DX'].min()) == -1 else 0.,
                                             optics_df['DX'].max()*1.1),
                             tools="box_zoom, pan, reset, hover, undo, zoom_in, zoom_out",
                             active_drag = 'box_zoom',
                             tooltips = tooltips)

    dispersion_plot.axis.major_label_text_font = 'times'
    dispersion_plot.axis.axis_label_text_font = 'times'
    dispersion_plot.axis.axis_label_text_font_style = 'normal'
    dispersion_plot.outline_line_color = 'black'
    dispersion_plot.sizing_mode = 'scale_width'
    dispersion_plot.toolbar.logo = None
    dispersion_plot.toolbar_location = None

    dispersion_plot.line('S', 'DX', source=optics, name='Dx', line_width=1.5, line_color='lime')

    layout_plot = figure(title="",
                         width=1000,
                         height=100,
                         x_range=optics_plot.x_range,
                         y_range=(-1.25, 1.25),
                         tools="box_zoom, pan, reset, hover, undo, zoom_in, zoom_out",
                         active_drag = 'box_zoom',
                         tooltips = tooltips_elements)

    layout_plot.axis.visible = False
    layout_plot.grid.visible = False
    layout_plot.outline_line_color = 'white'
    layout_plot.sizing_mode = 'scale_width'

    layout_plot.toolbar.logo = None
    layout_plot.toolbar_location = None

    plot_lattice_elements(layout_plot, optics_df)

    aperture_plot = figure(title="",
                           x_axis_label='s [m]',
                           y_axis_label=u'beam stay clear [\u03c3]',
                           width=1000,
                           height=300,
                           x_range=optics_plot.x_range,
                           y_range=Range1d(0, 50),
                           tools="box_zoom, pan, reset, hover, undo, zoom_in, zoom_out",
                           active_drag = 'box_zoom',
                           tooltips = tooltips_bsc)

    aperture_plot.axis.major_label_text_font = 'times'
    aperture_plot.axis.axis_label_text_font = 'times'
    aperture_plot.axis.axis_label_text_font_style = 'normal'
    aperture_plot.outline_line_color = 'black'
    aperture_plot.sizing_mode = 'scale_width'
    aperture_plot.toolbar.logo = None
    aperture_plot.toolbar_location = None

    aperture_plot.line('S', 'N1', source=aperture, name=u"\u03B2x", line_width=1.5, line_color='red')

    output_file(file_path, mode='inline')

    save(column([
                layout_plot,
                optics_plot,
                dispersion_plot,
                aperture_plot
                ],
                sizing_mode = 'scale_width'))

    return file_path


def plot_lattice_elements(figure, twiss):

    # create position column to correct for mad-x usually outputting the s of the end of an element
    twiss['POSITION']=twiss['S'] - 0.5*twiss['L']
    
    # modify lengths in order to plot zero-length elements
    twiss.loc[twiss['L']==0.0, 'L']=0.001

    for element in ELEMENT_STYLES:
        add_element_group(
            figure,
            twiss.loc[twiss.KEYWORD.str.contains(element['name'])],
            element['offset'],
            element['height'],
            element['color']
            )
    
    add_element_group(figure,
                      twiss.loc[(twiss.KEYWORD.str.contains('QUADRUPOLE')) & (twiss.K1L > 0.0)],
                      0.6,
                      1.2,
                      'red'
                      )
    
    add_element_group(figure,
                      twiss.loc[(twiss.KEYWORD.str.contains('QUADRUPOLE')) & (twiss.K1L < 0.0)],
                      -0.6,
                      1.2,
                      'blue'
                      )


def add_element_group(figure, twiss, offset, height, color):
    cds = ColumnDataSource(twiss)
    figure.rect(x='POSITION',
                y=offset,
                width='L',
                height=height,
                fill_color=color,
                line_color='black',
                source=cds)


if __name__ == "__main__":
    main()
