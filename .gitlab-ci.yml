# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Bash.gitlab-ci.yml

# See https://docs.gitlab.com/ee/ci/yaml/README.html for all available options


image: ubuntu:latest

# cache path where madx and sad executables will be saved
cache:
  paths:
    - ./codes

stages:
  - build
  - test
  - afs_pull
  - publish
  - deploy
  - trigger

.before_script_template:
  before_script:
    - echo "Before script section"
    - apt update -y
    - apt upgrade -y
    - apt install -y wget python3 python3-pip gcc gfortran bison git make patch libx11-dev groff-base
    - python3 -V
    - pip3 install --user -r python_requirements.txt


# BUILD STAGE ------------------------------------------------------------------
# check if the executables in the cache are older than 3 days (259200 seconds)
# if so, download latest version and build

get_madx_sad:
  extends: .before_script_template
  stage: build
  script:
    - if [[ -f codes/version.txt ]]; then read date1 < <(cat codes/version.txt); else date1=0; fi
    - if [[ $(( $(date -d "${CI_PIPELINE_CREATED_AT}" "+%s") - $(date -d "$date1" "+%s") )) < 86400 ]]; then exit 0; fi
    - echo "Get optics codes"
    - mkdir -p codes 
    - wget -q -O ./codes/madx https://madx.web.cern.ch/madx/releases/last-rel/madx-linux64-gnu && chmod u+x ./codes/madx
    - cd ./codes && rm -rf SAD
    - git clone https://github.com/KatsOide/SAD.git
    - cd ./SAD 
    - head -n 70 sad.conf > sad.conf.new && mv -f sad.conf.new sad.conf && make -s exe && make -s install
    - cd ../
    - echo "${CI_PIPELINE_CREATED_AT}" > version.txt
 


# TEST STAGE -------------------------------------------------------------------
# run test scripts and check output against reference parameters and other consistency checks
# saved log files, output, and plots in artifacts
  
test_t:
  extends: .before_script_template
  stage: test
  script:
    - echo "Run python tests on the madx and sad lattices and check if tunes, etc. agree"
    - mkdir -p public/t
    - cd ./tests
    - python3 run_codes.py --operation_mode t
    - python3 -m pytest --operation_mode=t
    - python3 ../toolkit/make_plots.py --operation_mode t
  artifacts:
    when: always
    paths:
      - ./public 
    expire_in: 1 week
  
test_z:
  extends: .before_script_template
  stage: test
  script:
    - echo "Run python tests on the madx and sad lattices and check if tunes, etc. agree"
    - mkdir -p public/z
    - cd ./tests
    - python3 run_codes.py --operation_mode z
    - python3 -m pytest --operation_mode=z
    - python3 ../toolkit/make_plots.py --operation_mode z
  artifacts:
    when: always
    paths:
      - ./public 
    expire_in: 1 week


# AFS STAGE -------------------------------------------------------------------
# update branches on afs

afs:
  stage: afs_pull
  image: gitlab-registry.cern.ch/linuxsupport/cc7-base
  allow_failure: true
  before_script:
      - yum install -y openssh-clients
      - mkdir -p ~/.ssh
      - 'echo "lxplus ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDTA/5AzXgbkSapknIPDoEePTM1PzIBSiyDnpZihdDXKzm8UdXxCDJLUVjBwc1JfBjnaXPEeBKZDuozDss/m98m5qQu+s2Dks000V8cUFTU+BFotzRWX0jWSBpmzse0477b40X2XCPqX0Cqfx9yHdkuMlyF0kJRxXgsGTcwzwbmvqNHJdHHYJJz93hGpBhYMREcDN5VOxXz6Ack3X7xfF29xaC91oOAqq75O11LXF5Y4kAeN9kDG8o6Zsqk4c5at5aqWqzZfnnVtGjhkgU2Mt5aKwptaFMe0Z3ys/zZM4SnsE9NfompnnWsiKk2y09UvrbzuYPWLt43Fp3+IFqRJvBX" > ~/.ssh/known_hosts'
      - 'echo -e "Host *\n\tGSSAPIDelegateCredentials yes\n\tGSSAPITrustDNS yes\n\n" > ~/.ssh/config'
  script:
      - echo "${EOS_ACCOUNT_PASSWORD}" | kinit -f ${EOS_ACCOUNT_USERNAME}@CERN.CH
      - klist
      - ssh ${EOS_ACCOUNT_USERNAME}@lxplus "cd /afs/cern.ch/eng/acc-models/fcc/fccee/${CI_COMMIT_REF_NAME}/;git pull"
  only:
    - /^V\d\d$/
    - tags

# PUBLISH STAGE -------------------------------------------------------------------
# if tagged, the repository is uploaded to zenodo

send-snapshot:
  stage: publish
  image: python:3.6
  script:
    - echo "${CI_COMMIT_TAG}"
    - if [[ ! $CI_COMMIT_TAG =~ ^V?[0-9]+\.?[0-9]+ ]]; then exit 0; fi
    - pip install gitlab2zenodo
    - git archive --format zip --output ${CI_COMMIT_TAG}.zip ${CI_COMMIT_TAG}
    - g2z-send -m .zenodo.json ${CI_COMMIT_TAG}.zip
  only:
    - tags

# using the output from the tests, the files for the website will be updated
# the full repository is moved inside the artifact for later deployment to eos by the deployment stage

web_content:
  extends: .before_script_template
  stage: publish
  script:
    - python3 --version
    - python3 web/create_web.py --branch ${CI_COMMIT_REF_NAME}
    - shopt -s extglob
    - mv !(*codes*|*public*) ./public/
  only:
    - /^V\d\d$/
    - tags
  artifacts:
    paths:
      - ./public
    expire_in: 15 mins


# DEPLOY STAGE -------------------------------------------------------------------
# files for the website create in the web_content workflow will be uploaded to the
# eos website directory, in a folder with the branch name

deployment:
  stage: deploy
  image: gitlab-registry.cern.ch/ci-tools/ci-web-deployer:latest
  variables:
    EOS_PATH: /eos/project/a/acc-models/public/fcc/fccee/${CI_COMMIT_REF_NAME}/
  only:
    - /^V\d\d$/
    - tags
  script:
    - deploy-eos

trigger:
  # trigger pipeline in the main repo to execute mkdocs build on EOS
  image: gitlab-registry.cern.ch/acc-models/acc-models-www/cern_cc7_base
  stage: trigger
  only:
    - /^V\d\d$/
  script:
    - "curl -X POST -F token=${WEB_BUILD_TRIGGER_TOKEN} -F ref=master https://gitlab.cern.ch/api/v4/projects/76508/trigger/pipeline"
