
# FCC-ee optics repository toolkit directory

Collection of scripts that help in common first analysis steps.
Scripts are written to work in conjunction with the example sad/MAD-X scripts in the `examples` or `tests` folder.

## Prerequisites

Scripts with the file extension `.n` are SAD modules, which have to be imported into the main SAD script via

```
Get["../toolkit/MODULE.n"];
```

and then the function can be called via

```
Function[Parameter];
```

Scripts ending on `.py` are stand-alone `python3.7` scripts, used to post-process data.
To run those, several packages need to be installed, which is done via

```bash
pip install -r python_requirements.txt
```

Scripts ending on `.madx` are `MAD-X` scripts, used to post-process data.

## Scripts

- *SAD Scripts*
    - `SAD2MADX.n` - converts the currently active SAD sequence into MAD-X format and saves it into specified `fname`.
    - `tunematching.n` - matches the tunes `n_x` & `n_y` of the FCC-ee collider ring to the specified values using the quadrupoles in the RF-straights.
    - `twiss.n` - saves the last optics functions from SAD's `CALCULATE;` to a `twiss`-file `filename`.
    - `closed_orbit.n` - prints the closed orbit from SAD's `Emittance[];` to a `twiss` like file with the name `filename`.
- *Python Scripts*
    - `plot_da.py` - provided the DA from the `check_da.sad` script, plots the dynamic aperture and optionally saves the data in `.tfs` format.
    - `plot_lattice.py` - given a `twiss`-file from one of the `optics`-scripts in the `tests` folder, plots the optics functions and saves them as `.png`.
    - `plot_aperture.py` - provided a `aperture`-file from from MAD-X, the beamstay clear, beam pipe radius, and momentum acceptance are plotted.
    - `make_plots.py` - a wrapper script for the three `python` plot script above, used in the `test_*` stage.
- *MAD-X Scripts*
    - `trackMacros.madx` - contains MAD-X macros to perform DA and MA tracking with `MAD-X-PTC`.
