## FCC-ee changelog

#### V22.x
    - Reduce beta_x^* to 10 cm at Z. More information [here](https://indico.cern.ch/event/1118299/contributions/4766789/attachments/2409541/4122785/Bx%2A10cm_Oide_220317.pdf).

#### V22.1 2022-03-10
    - 4-IP Lattice for new layout PA31-1.0 with 91km circumference. 
    - Phase advance of arc cells changed to 90/90. 
    - 4 RF sections installed in 2.1km long straight sections.
More information in the [WP2presentation](https://indico.cern.ch/event/1085318/contributions/4582685/subcontributions/356824/attachments/2354474/4018133/Optics_since_CDR_Oide_211129.pdf)
