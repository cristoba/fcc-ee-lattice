# FCC-ee optics repository 

The FCC-ee optics and data repository holds data and optics files for a number of beam dynamics studies under way to design the FCC-ee collider.

This includes lattice files detailing the layout of the machine and the optics, as well as files containing numerous parameters associated with each operation mode.

## Accessing and referencing the repository

The repository can be found under this [link](https://gitlab.cern.ch/acc-models/fcc/fcc-ee-lattice).
The data can also be found on `/afs/cern.ch/eng/acc-models/fccee` and on `/eos/project/a/acc-models/public/fcc/fccee`.

Following the mandated regulations of the Horizon 2020 research and innovation programme, the repository and its files are available without any access restriction.

Following certain project milestones, a snapshot of the repository will be tagged with a version number, and a reference for publication including a DOI will be provided via Zenodo.

## Structure

Only a few files are located at the top level of the repository, mostly for administration of the repository itself.
Most important are `README.md` that serves as a quick introduction and orientation guide, the `CHANGELOG.md` for a running count of changes, and `reference_parameters.json` that contains the reference set of machine parameters for each operation mode, such as the beam energy, tunes, number of bunches and bunch population.

Most users will find the necessary files in the directories below.

### lattices

This directory holds the lattice files for the different operation modes of the collider.

### aperture

In this directory, files required for collimation related studies, such as aperture files, are stored.

### examples

Small, self contained example files can be found here. They are intended as a help for users to start their own studies and show *i.e.* how to load the lattice files and plot the optics, or how to setup a matching or tracking study.

### toolkit

Commonly functions and subroutines are put in this folder.

### web

Contains scripts and templates for deploying the information to the [Acc-Models website](https://acc-models.web.cern.ch/acc-models/).

### test

To check that the models and parameter set stay consistent after changes, a number scripts are kept that here and are run after each change and report in case of inconsistencies.

## Development

The version control system of GIT is used, and is centrally managed by a [gitlab instance](https://gitlab.cern.ch/) hosted at CERN.

Major milestones in the development of the FCC-ee lattices will be marked with a version.
The current state of the repository will be saved, and available under `Tags` in GIT, or alternatively on Zenodo, where a `DOI` will be attached to the version.
The following scheme for versioning is used **VYY.idx**, where

- *YY* is the year in which the first version is released
- and *idx* a running index to distinguish between minor updates.

For each released version, where the identifier **VYY** is versioned, a separate branch is created and protected, that is, only maintainers can perform any change on this branch.

This branch will contain the latest version (latest *idx*) including potentially unreleased changes.

### For contributors

Contributors are encouraged to create a separate branch to contribute their own work to the repository and refrain from modifying the main branches.

Contributor branches should be named after the released version that serves as the basis (target branch) with an additional descriptor to facilitate the organisation: **VYY_description**

To merge the changes into the target branch, a merge request shall be created. This will trigger a review and approval process by maintainers. Once all checks are passed, the changes will be integrated in the target branch.

In case of any question or problem, please do not hesistate to contact [FCC.Optics@cern.ch](mailto:FCC.Optics@cern.ch).
