from pathlib import Path
import pytest
import json
import pandas as pd
from pandas._testing import assert_frame_equal, assert_series_equal
import tfs
import numpy as np
import scipy.interpolate as interpld


REPOSITORY_TOP_LEVEL = Path(__file__).resolve().parent.parent
REFERENCE_FILE = json.load(open(REPOSITORY_TOP_LEVEL/"reference_parameters.json"))
TWISS_DIR = REPOSITORY_TOP_LEVEL/'public'

TUNE_ACCURACY = REFERENCE_FILE['TUNE_ACCURACY']
EMITTANCE_ACCURACY = REFERENCE_FILE['EMITTANCE_ACCURACY']
BETA_ACCURACY = REFERENCE_FILE['BETA_ACCURACY']

PLANES = ['X', 'Y', 'Z']


def test_emit(operation_mode):
    twiss=tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_b1_nottapered.tfs", index="NAME")
    assert abs(twiss.headers['EX'] - REFERENCE_FILE[operation_mode]['EMITTANCE_X'])/REFERENCE_FILE[operation_mode]['EMITTANCE_X'] < EMITTANCE_ACCURACY
    assert abs(twiss.headers['EY'] - REFERENCE_FILE[operation_mode]['EMITTANCE_Y'])/REFERENCE_FILE[operation_mode]['EMITTANCE_Y'] < EMITTANCE_ACCURACY
    assert abs(twiss.headers['EY']/twiss.headers['EX']) - REFERENCE_FILE[operation_mode]['EMITTANCE_RATIO'] < EMITTANCE_ACCURACY
    assert abs(twiss.headers['PC'] - REFERENCE_FILE[operation_mode]['ENERGY']) < TUNE_ACCURACY


def test_tune(operation_mode):
    twiss=tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_b1_nottapered.tfs", index="NAME")
    assert abs(twiss.headers['Q1']%1 - REFERENCE_FILE[operation_mode]['Q1']) < TUNE_ACCURACY
    assert abs(twiss.headers['Q2']%1 - REFERENCE_FILE[operation_mode]['Q2']) < TUNE_ACCURACY


def test_betastar(operation_mode):
    twiss=tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_b1_nottapered.tfs", index="NAME")
    for ip in ['IP.1','IP.2','IP.3','IP.4']:
        assert abs(twiss.loc[ip,'BETX'] - REFERENCE_FILE[operation_mode]['BETASTAR_X']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'BETY'] - REFERENCE_FILE[operation_mode]['BETASTAR_Y']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'ALFX']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'ALFY']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'DX']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'DY']) < BETA_ACCURACY
 

def test_symmetry(operation_mode):
    # load twiss and remove collimators and drifts, as those lead to an asymmetry in the dataframe indices
    twiss=tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_b1_nottapered.tfs", index='NAME')
    mask = twiss.index.str.contains('TCP|TCS|DRIFT_', regex=True)
    twiss=twiss[~mask]
    
    # assume two fold symmetry
    # compare on optics functions, but drop names as those are different left to right
    optics_parameters=('BETX', 'BETY', 'ALFX', 'ALFY', 'DX')
    assert_frame_equal(twiss.loc['IP.1':'IP.2', optics_parameters].reset_index().drop(labels=['NAME'], axis=1), 
                       twiss.loc['IP.3':'IP.4', optics_parameters].reset_index().drop(labels=['NAME'], axis=1),
                       check_exact=False, atol=BETA_ACCURACY, rtol=0.01)


def test_aperture(operation_mode):
    aperture=tfs.read(TWISS_DIR/operation_mode/f"aperture_{operation_mode}_b1_nottapered.tfs", index='NAME')
    
    assert aperture.headers['N1MIN'] > min(REFERENCE_FILE[operation_mode]['APERTURE']['PRIMARY_COLLIMATOR_APERTURE_X'],
                                           REFERENCE_FILE[operation_mode]['APERTURE']['PRIMARY_COLLIMATOR_APERTURE_Y'])
    # assert 'TCP' in aperture.headers['AT_ELEMENT']
    mask = aperture.index.str.contains('TCP|TCS', regex=True)
    aperture=aperture[~mask]
    assert all(aperture['N1']>REFERENCE_FILE[operation_mode]['APERTURE']['N1'])
    aperture = calc_momentum_acceptance(aperture, REFERENCE_FILE[operation_mode]['APERTURE']['NSIG_FOR_MOMENTUM_ACCEPTANCE'])

    assert all(aperture['MOMENTUM_ACCEPTANCE'][aperture.APER_1>0.0] > REFERENCE_FILE[operation_mode]['APERTURE']['MIN_MOMENTUM_ACCEPTANCE'])


def calc_momentum_acceptance(df, n_sig):

    df['MOMENTUM_ACCEPTANCE'] = (df['APER_1'] - n_sig*np.sqrt(df['BETX']*df.headers['EXN']/(df.headers['BETA']*df.headers['GAMMA'])) )/ abs(df['DX'])

    return df


def test_survey(operation_mode):
    survey=tfs.read(TWISS_DIR/operation_mode/f"survey_madx_{operation_mode}_b1.tfs")

    # check that ring is closed by comparing X,Y,Z of start and endpoint
    assert_series_equal(survey[PLANES].iloc[0],
                       survey[PLANES].iloc[-1],
                       check_exact=False, atol=BETA_ACCURACY, rtol=0.01, check_names=False) 
    survey = survey.set_index('NAME')
    xcenter_fccee = (survey.loc['IP.1','X']+survey.loc['IP.2','X'])*0.5
    zcenter_fccee = (survey.loc['IP.1','Z']+survey.loc['IP.2','Z'])*0.5
    Rvatan_fccee = interpld.interp1d(
                                     np.arctan2((survey['Z']-zcenter_fccee),
                                                (survey['X']-xcenter_fccee)),
                                     np.sqrt((survey['X']-xcenter_fccee)**2+(survey['Z']-zcenter_fccee)**2),
                                     fill_value="extrapolate"
                                     )
    theta = np.linspace(-np.pi, np.pi, 1001, endpoint=False)
