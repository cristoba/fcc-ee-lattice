from pathlib import Path
import pytest
import json
from pandas._testing import assert_frame_equal, assert_series_equal
import tfs
import toolkit.plot_da as sad_da
import ast
import numpy as np

REPOSITORY_TOP_LEVEL = Path(__file__).resolve().parent.parent
REFERENCE_FILE = json.load(open(REPOSITORY_TOP_LEVEL/"reference_parameters.json"))
TWISS_DIR = REPOSITORY_TOP_LEVEL/'public'

TUNE_ACCURACY = REFERENCE_FILE['TUNE_ACCURACY']
BETA_ACCURACY = REFERENCE_FILE['BETA_ACCURACY']

PLANES = ('X', 'Y', 'Z')
SURVEY_PLANES = [f"GEO_{plane}" for plane in PLANES]


def test_tune(operation_mode):
    twiss=tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_sad.tfs", index="NAME")    
    assert abs(twiss.headers['Q1']%1 - REFERENCE_FILE[operation_mode]['Q1']) < TUNE_ACCURACY
    assert abs(twiss.headers['Q2']%1 - REFERENCE_FILE[operation_mode]['Q2']) < TUNE_ACCURACY


def test_betastar(operation_mode):
    twiss=tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_sad.tfs", index="NAME")
    for ip in ['IP.1','IP.2','IP.3','IP.4']:
        assert abs(twiss.loc[ip,'BETX'] - REFERENCE_FILE[operation_mode]['BETASTAR_X']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'BETY'] - REFERENCE_FILE[operation_mode]['BETASTAR_Y']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'ALFX']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'ALFY']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'DX']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'DY']) < BETA_ACCURACY
 

def test_symmetry(operation_mode):
    # load twiss and remove collimators and drifts, as those lead to an asymmetry in the dataframe indices
    twiss=tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_sad.tfs", index="NAME")
    mask = twiss['KEYWORD'].str.contains('DRIFT', regex=True)
    twiss=twiss[~mask]
    
    # assume two fold symmetry
    # compare on optics functions, but drop names as those are different left to right
    optics_parameters=('BETX', 'BETY', 'ALFX', 'ALFY', 'DX')
    assert_frame_equal(twiss.loc['IP.1':'IP.2', optics_parameters].reset_index().drop(labels=['NAME'], axis=1), 
                       twiss.loc['IP.3':'IP.4', optics_parameters].reset_index().drop(labels=['NAME'], axis=1),
                       check_exact=False, atol=BETA_ACCURACY, rtol=0.01)


def test_survey(operation_mode):
    survey=tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_sad.tfs", index='NAME')

    # check that ring is closed by comparing X,Y,Z of start and endpoint
    assert_series_equal(survey[SURVEY_PLANES].iloc[0],
                       survey[SURVEY_PLANES].iloc[-1],
                       check_exact=False, atol=BETA_ACCURACY, rtol=0.01, check_names=False) 


def test_transverse_da(operation_mode):  
    da_df = sad_da.process_da_file(TWISS_DIR/operation_mode/f"daxy_{operation_mode}_sad.tfs",
                                   'XY',
                                   REFERENCE_FILE[operation_mode]['EMITTANCE_RATIO'],
                                   None)
    # check ranges for SAD DA study
    for plane in PLANES:
        da_range = np.array(ast.literal_eval(da_df.headers[f'{plane}Range']))
        da_range = da_range if not plane=='Y' else da_range/np.sqrt(REFERENCE_FILE[operation_mode]['EMITTANCE_RATIO'])

        assert [da_range[0],da_range[-1]] == REFERENCE_FILE[operation_mode]['DA_TRANSVERSE'][f'{plane}_RANGE']

    # check that DA at x=0 is larger than MIN_DA_Y
    assert da_df.loc[0, 'DA'] >= REFERENCE_FILE[operation_mode]['DA_TRANSVERSE']['MIN_DA_Y']
    # check that for y=0 in given x range number of survived turns is larger or equal than reference
    for x in range(REFERENCE_FILE[operation_mode]['DA_TRANSVERSE']['MIN_DA_X'][0],
                   REFERENCE_FILE[operation_mode]['DA_TRANSVERSE']['MIN_DA_X'][1]+1,2):
        assert da_df.loc[x, 'SIGMAY_0']>= REFERENCE_FILE[operation_mode]['DA_TRANSVERSE']['TURNS']


def test_longitudinal_da(operation_mode):  
    da_df = sad_da.process_da_file(TWISS_DIR/operation_mode/f"daxz_{operation_mode}_sad.tfs",
                                   'ZX',
                                   REFERENCE_FILE[operation_mode]['EMITTANCE_RATIO'],
                                   None)

    # check ranges for SAD DA study
    for plane in PLANES:
        da_range = np.array(ast.literal_eval(da_df.headers[f'{plane}Range']))
        da_range = da_range if not plane=='Y' else da_range/np.sqrt(REFERENCE_FILE[operation_mode]['EMITTANCE_RATIO'])

        assert [da_range[0],da_range[-1]] == REFERENCE_FILE[operation_mode]['DA_LONGITUDINAL'][f'{plane}_RANGE']

    # check that DA at z=0 is larger than MIN_DA_X
    assert da_df.loc[0, 'DA'] >= REFERENCE_FILE[operation_mode]['DA_LONGITUDINAL'][f'MIN_DA_X']
    # check that x/y=0 in given z range number of survived turns is larger or equal than reference
    for z in range(REFERENCE_FILE[operation_mode]['DA_LONGITUDINAL']['MIN_DA_Z'][0],
                   REFERENCE_FILE[operation_mode]['DA_LONGITUDINAL']['MIN_DA_Z'][1]+1):
        assert da_df.loc[z, 'SIGMAX_0']>= REFERENCE_FILE[operation_mode]['DA_LONGITUDINAL']['TURNS']    
