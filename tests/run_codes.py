from pathlib import Path
import json
from generic_parser import EntryPointParameters, entrypoint
import jinja2
import subprocess

REPOSITORY_TOP_LEVEL = Path(__file__).resolve().parent.parent
REFERENCE_FILE = json.load(open(REPOSITORY_TOP_LEVEL/"reference_parameters.json"))
TEMPLATE_DIRECTORY = REPOSITORY_TOP_LEVEL/'tests'/'templates'

MADX_SCRIPTS = {
    'executable': REPOSITORY_TOP_LEVEL/'codes'/'madx',
    'templates': ['madx.template'],
    }

SAD_SCRIPTS = {
    'executable': REPOSITORY_TOP_LEVEL/'codes'/'SAD'/'bin'/'gs',
    'templates': ['sad_optics.template', 'sad_da.template'],
    }

SCRIPTS = [MADX_SCRIPTS, SAD_SCRIPTS]


# Script arguments -------------------------------------------------------------
def get_params():
    params = EntryPointParameters()
    params.add_parameter(
        name="operation_mode",
        type=str,
        required=True,
        choices=['z','w','h','t'],
        help="Define which operation mode should be tested: z,w,h,t",
    )

    return params


@entrypoint(get_params(), strict=True)
def main(opt):
    working_directory = REPOSITORY_TOP_LEVEL/'public'/opt.operation_mode
    for script in SCRIPTS:
        for template in script['templates']:
            fill_template(template, opt.operation_mode, working_directory)
            run_script(script['executable'], template, working_directory)


def fill_template(template_file, operation_mode, working_directory):
    loader = jinja2.FileSystemLoader(searchpath=TEMPLATE_DIRECTORY)
    env = jinja2.Environment(loader=loader, undefined=jinja2.StrictUndefined)
    template = env.get_template(template_file)
    with open(working_directory/return_filled_mask_name(template_file), 'w') as f:
        f.write(template.render(reference=REFERENCE_FILE[operation_mode], operation_mode=operation_mode))
        

def run_script(executable, template_file, working_directory):
    with open(working_directory/return_log_name(template_file), 'w') as f: 
        subprocess.run([executable, return_filled_mask_name(template_file)], check=True, stdout=f, cwd=working_directory)


def return_filled_mask_name(template_file):
    return Path(template_file).with_suffix('').with_suffix('.rendered')


def return_log_name(template_file):
    return Path(template_file).with_suffix('').with_suffix('.log')


if __name__ == "__main__":
    main()